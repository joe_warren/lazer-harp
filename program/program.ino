#include <Bounce.h>  // Bounce library makes button change detection easy
const int channel = 1;
const int ledPin = LED_BUILTIN;

Bounce button1 = Bounce(1, 50);  // 5 = 5 ms debounce time
Bounce button2 = Bounce(2, 50);  // which is appropriate for good
Bounce button3 = Bounce(3, 50);  // quality mechanical pushbuttons
void setup() {
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  button1.update();
  button2.update();
  button3.update();
  // Note On messages when each button is pressed
  if (button1.risingEdge()) {
    usbMIDI.sendNoteOn(60, 99, channel);  // 60 = C4
    digitalWrite(ledPin, HIGH);
  }
  if (button2.risingEdge()) {
    usbMIDI.sendNoteOn(61, 99, channel);  // 61 = C#4
  }
  if (button3.risingEdge()) {
    usbMIDI.sendNoteOn(62, 99, channel);  // 62 = D4
  }
  // Note Off messages when each button is released
  if (button1.fallingEdge()) {
    usbMIDI.sendNoteOff(60, 0, channel);  // 60 = C4
    digitalWrite(ledPin, LOW);
  }
  if (button2.fallingEdge()) {
    usbMIDI.sendNoteOff(61, 0, channel);  // 61 = C#4
  }
  if (button3.fallingEdge()) {
    usbMIDI.sendNoteOff(62, 0, channel);  // 62 = D4
  }

  // MIDI Controllers should discard incoming MIDI messages.
  while (usbMIDI.read()) {
  }
}
