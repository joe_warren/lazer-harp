{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Object where

import qualified Csg
import qualified Csg.STL
import Control.Monad
import Data.List
import Data.Serialize
import Data.Semigroup
import Data.Vec3 as V3
import qualified Data.Text.IO as T

nStrings :: Int
nStrings = 24

stringLength:: Double
stringLength = 8

width :: Double
width = 50

wallThickness :: Double
wallThickness = 3

holeHeightForBottom :: Double
holeHeightForBottom  = 20
cavityHeightForBottom :: Double
cavityHeightForBottom = 50

holeHeightForTop :: Double
holeHeightForTop = 3
cavityHeightForTop :: Double
cavityHeightForTop = 14

middleLength :: Double
middleLength = stringLength * fromIntegral nStrings

rodR :: Double
rodR = 3
outerR :: Double
outerR = 10

boltR :: Double
boltR = 0.1 + 11/2
boltD :: Double
boltD = 6

resistorR :: Double
resistorR = 6/2
resistorW :: Double 
resistorW = 4.1

baseMount ::  Double -> Csg.BspTree
baseMount height = Csg.subtract (
              Csg.union
                (Csg.translate (offset, 0, 0) $ 
                  Csg.unionConcat [ Csg.rotate (0, 0, 1) (i - pi/6) shape | i <- [0, 2 * pi/3, 4 * pi/3]]
                )
                (Csg.scale (boltR+wallThickness, width, height) $ Csg.translate (0.5, 0, 0.5) Csg.unitCube)
              ) (
                Csg.unionConcat [
                  Csg.translate (offset, 0, 0) $ 
                    Csg.unionConcat [ Csg.rotate (0, 0, 1) (i - pi/6) rod | i <- [0, 2 * pi/3, 4 * pi/3]],
                  (Csg.scale (1000, 1000, 1000) $ Csg.translate (-0.5, 0, 0.5) Csg.unitCube),
                  Csg.unionConcat [ Csg.translate (offset, 0, h) $ Csg.rotate (0, 0, 1) (i - pi/6) bolt | i <- [0, 2 * pi/3, 4 * pi/3], h <- [0, height]]
              ])
  where
    offset = boltR + wallThickness+  0.5*d* (sin (pi/6))
    cyl = Csg.scale (outerR, outerR, height) $ Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 16
    d = width - outerR*2
    cub = Csg.rotate (0, 0, 1) (pi/6) $ Csg.scale (outerR *2, d, height) $ Csg.translate (0, 0.5, 0.5) $ Csg.unitCube
    positionColumn = Csg.translate (0,  - 0.5 * d / (cos (pi/6)),0)
    shape = positionColumn $ Csg.union cyl cub
    rod = positionColumn $ Csg.scale (rodR, rodR, height * 3) $ Csg.unitCylinder 16
    bolt = positionColumn $ Csg.scale (boltR, boltR, boltD*2) $ Csg.unitCylinder 6

wireHole :: Double -> Double -> Csg.BspTree
wireHole height holeHeight = Csg.translate (offset, 0, holeHeight) $ Csg.union (
             Csg.scale (holeR, holeR, height) $ Csg.translate (0, 0, -0.5) $ Csg.unitCylinder 16
            ) (
             Csg.scale (offset*2, holeR*2, holeR*2) $ Csg.translate (-0.5, 0, 0.5)  Csg.unitCube
            ) 
  where 
    holeR = 4
    d = width - outerR*2
    offset = boltR + wallThickness+  0.5*d* (sin (pi/6))

lidScrewR :: Double
lidScrewR = 2.5/2
lidScrewD :: Double
lidScrewD = 4

lidScrewCountersinkR :: Double
lidScrewCountersinkR = 5/2
lidScrewCountersinkD :: Double
lidScrewCountersinkD = 2

lidThickness :: Double 
lidThickness = 1
lidRecess :: Double
lidRecess = 1

base :: Double -> Double -> Csg.BspTree
base cavityHeight holeHeight = Csg.unionConcat [
    Csg.subtract (
        Csg.scale (middleLength , width , height) $ Csg.translate (0, 0, 0.5) Csg.unitCube
      ) (
        Csg.unionConcat [
          -- cavity
          Csg.translate (0, 0, holeHeight) $ Csg.scale ( middleLength*2 , width-2*wallThickness , height) $ Csg.translate (0, 0, 0.5) Csg.unitCube,
        
          -- lid recess
          Csg.translate (0, 0, height) $ 
            Csg.scale (middleLength*2, width+2*(lidRecess-wallThickness), lidThickness) $ 
              Csg.translate (0, 0, -0.5) Csg.unitCube,

          -- screw holes for lid
          Csg.unionConcat [ --
            Csg.translate (i* middleLength /3, 0, height - lidScrewD) $ 
              Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (lidScrewR, lidScrewR, width * 10) $
                Csg.unitCylinder 16
            | i <- [-1, 1]], 
          -- and countersink them
          Csg.unionConcat [
            Csg.translate (i* middleLength /3, j * width/2, height - lidScrewD) $ 
              Csg.rotate (1, 0, 0) (pi/2) $ 
                Csg.scale (lidScrewCountersinkR, lidScrewCountersinkR, lidScrewCountersinkD * 2) $
                Csg.unitCylinder 16
            | i <- [-1, 1], j <- [-1, 1]]
        ]
      ),
    Csg.translate (middleLength* 0.5, 0, 0) $ ((baseMount height) `Csg.subtract` (wireHole height holeHeight)),
    Csg.rotate (0, 0, 1) (pi) $ Csg.translate (middleLength* 0.5, 0, 0) $ baseMount height
  ]
  where 
    height = holeHeight + cavityHeight

lid :: Csg.BspTree
lid = Csg.translate (0, 0, cavityHeightForTop + holeHeightForTop) $ 
        Csg.subtract (
          Csg.unionConcat $ 
            (pure $ Csg.scale (middleLength, lidWidth, lidThickness) $
            Csg.translate (0, 0, -0.5) Csg.unitCube) <>
            [ --
              Csg.translate (i* middleLength /3, 0, -lidScrewD) $ 
                Csg.rotate (1, 0, 0) (pi/2) $ 
                  Csg.scale (someRadius, someRadius, cavityWidth ) $
                    Csg.unitCylinder 16
                | i <- [-1, 1]] <>
            [ --
              Csg.translate (i* middleLength /3, 0, -0) $ 
                Csg.scale (someRadius * 4, cavityWidth, lidScrewD) $
                  Csg.translate (0, 0, -0.5) $ 
                    Csg.unitCube
                | i <- [-1, 1]]
        ) (
          -- screw holes
          Csg.unionConcat $ [ --
            Csg.translate (i* middleLength /3, 0, -lidScrewD) $ 
              Csg.rotate (1, 0, 0) (pi/2) $ Csg.scale (lidScrewR, lidScrewR, width * 10) $
                Csg.unitCylinder 16
            | i <- [-1, 1]] <>
            -- bevel 
            [ Csg.translate (i* middleLength /3 + j * 2 * someRadius, 0, -lidScrewD) $ 
              Csg.rotate (1, 0, 0) (pi/2) $ 
                Csg.scale (someRadius, someRadius, width * 10) $
                  Csg.unitCylinder 16
              | i <- [-1, 1], j <- [-1, 1]]
        )
  where
    lidWidth = width + 2 * (lidRecess - wallThickness)
    someRadius = lidScrewD - lidThickness
    cavityWidth = width - 2*wallThickness

resistorHoles :: Csg.BspTree
resistorHoles = positionHoles (const True) $ 
      Csg.intersection (
          Csg.scale (resistorR, resistorR, holeHeightForBottom * 2) $
            Csg.unitCylinder 16
        ) (
          Csg.scale (resistorW, resistorR*2, holeHeightForBottom*10) $
            Csg.unitCube
        )



teensyMount :: Csg.BspTree
teensyMount = Csg.translate (-middleLength/2, width/2 - wallThickness, relativeHeight) $
                (Csg.union 
                    ( Csg.scale (tD, tL+4, tW+4) $ Csg.translate (0.5, -0.5, 0) Csg.unitCube)
                    ( Csg.translate (0, 0,-tW/2 - 2)$
                        Csg.intersection 
                          (Csg.rotate (0, 1, 0) (pi/4) $
                            Csg.scale (tD*sqrt 2, tL+4, tD * sqrt 2) $ 
                              Csg.translate (0, -0.5, 0) Csg.unitCube
                           )(Csg.uniformScale 100 $ Csg.translate (0.5, 0, 0) Csg.unitCube)
                    )
                ) `Csg.subtract`
                (Csg.scale (tD, tL, tW) $ Csg.translate (0.5, -0.5, 0) Csg.unitCube)
    where 
        tW = 18
        tL = 35.5
        tD = 5
        relativeHeight = cavityHeightForBottom + holeHeightForBottom - tW/2 - 2 - 1

teensyHole :: Csg.BspTree
teensyHole = Csg.translate (-middleLength/2, width/2 - wallThickness, relativeHeight) $
    (Csg.scale (tD, 10, usbL) $ Csg.translate (0.5, 0, 0) Csg.unitCube) `Csg.union`
    (Csg.translate (tD/2, 1, 0) $ Csg.rotate (1, 0, 0) (-pi/2) $ Csg.scale (r, r, 10) $ 
        Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 16)
  where
        tD = 5
        usbL = 8
        tW = 18
        r = 6
        relativeHeight = cavityHeightForBottom + holeHeightForBottom - tW/2 - 2 - 1

buttonHoles = Csg.unionConcat $ [
                Csg.translate (i*sep/2, width/2, holeHeightForBottom + sep/2) $
                  Csg.scale (butSize, 20, butSize) $
                   Csg.unitCube
                | i<- [-3, -1, 1, 3]] <> [
                  Csg.translate (0, width/2 - butWall, holeHeightForBottom + sep/2) $
                    Csg.scale (sep*3 + butSize + 2, 10, butSize + 2) $
                      Csg.translate (0, -0.5, 0) $ Csg.unitCube
                ]
  where 
    butSize = 14
    sep = 20
    butWall = 2

veroboardSlot :: Csg.BspTree
veroboardSlot = 
    Csg.unionConcat [
      Csg.translate (0, i* (width/2 -wallThickness), h) $
        Csg.subtract
         (Csg.intersection (
            Csg.rotate (0, 0, 1) (pi/4) $ Csg.scale (edge, edge, l) $ Csg.unitCube
         ) (
            Csg.rotate (1, 0, 0) (pi/4) $ Csg.scale (10, l / sqrt 2, l / sqrt 2) $ Csg.unitCube
         ))
         (Csg.scale (veroboardWidth, 10, l*2) $ Csg.unitCube)
    | i <-[-1, 1]]
  where
    veroboardWidth = 1.5
    l = 20
    edge = wallThickness * sqrt 2
    h = cavityHeightForBottom + holeHeightForBottom - l/2 - 8

veroboardSlots = positionHoles ((==0) . (`mod` 2)) veroboardSlot

bottomSection :: Csg.BspTree
bottomSection = Csg.unionConcat [
    base cavityHeightForBottom holeHeightForBottom, 
    teensyMount,
    veroboardSlots
    ]`Csg.subtract` (
        Csg.unionConcat [
            resistorHoles,
            teensyHole, 
            buttonHoles
        ])

positionHoles :: (Int -> Bool) -> Csg.BspTree -> Csg.BspTree
positionHoles f hole = 
 Csg.unionConcat [
    Csg.translate ((fromIntegral i) * stringLength - offset, 0, 0) hole
  | i <- filter f [1..nStrings]]
  where 
    offset = (1 + fromIntegral nStrings) * stringLength * 0.5


lazerR :: Double
lazerR = 6/2

lazerMount :: Csg.BspTree
lazerMount = (Csg.unionConcat $ [
    Csg.scale (lazerR + 0.8, lazerR + 0.8, d) $ Csg.unitCylinder 16,
    Csg.translate (0, 2, 0) $ Csg.scale (w, 20, d) $ Csg.unitCube, 
    Csg.translate (0, -8, 0) $ Csg.scale (8, w, d) $ Csg.unitCube
  ] <> (
    Csg.translate <$> lazerMountCenters <*> 
        (pure $ Csg.scale (2.5/2 + 0.8,2.5/2 + 0.8,d) $ Csg.unitCylinder 8)
  )) `Csg.subtract` (Csg.unionConcat 
    [Csg.scale (lazerR, lazerR, d*2) $ Csg.unitCylinder 16,
     lazerMountHoles
    ])
 where
  w = 3
  d = 2

lazerMountsPositioned :: Csg.BspTree
lazerMountsPositioned = Csg.translate (0, 0, -10) $ Csg.union (
    positionHoles ((==0).(`mod` 2)) lazerMount
    --positionHoles ((==14)) lazerMount
  ) (
    positionHoles ((==1).(`mod` 2)) $ Csg.rotate (0, 0, 1) pi lazerMount
    --positionHoles ((==13)) $ Csg.rotate (0, 0, 1) pi lazerMount
  )

lazerMountCenters :: [(Double, Double, Double)]
lazerMountCenters = [
        ( 0,  12, 0),
        (-4, -8, 0),
        ( 4, -8, 0)
    ]

lazerMountHoles :: Csg.BspTree
lazerMountHoles = Csg.unionConcat $ (
    Csg.translate <$> lazerMountCenters <*> pure bolt
    )
    where 
        bolt = Csg.scale (2.5/2,2.5/2,100) $ Csg.unitCylinder 8

lazerHoles :: Csg.BspTree
lazerHoles = Csg.unionConcat [
    positionHoles (const True) $ 
      Csg.intersection (
        Csg.scale (lazerR+2, lazerR+2, holeHeightForTop * 2) $
          Csg.unitCylinder 16
      )(Csg.scale (2*(lazerR+0.5), 2*(lazerR+2), holeHeightForTop * 2) $
          Csg.unitCube
      ),
    positionHoles ((==0).(`mod` 2)) lazerMountHoles,
    positionHoles ((==1).(`mod` 2)) $ Csg.rotate (0, 0, 1) pi lazerMountHoles
  ] 


topSection :: Csg.BspTree
topSection = base cavityHeightForTop holeHeightForTop `Csg.subtract` lazerHoles


paths :: [(String, Csg.BspTree)]
paths = [
    ("lazer_harp.stl",                          , bottomSection                 ),
    ("lazer_harp_top.stl"                       , topSection                    ),
    ("lazer_harp_lid.stl"                       , lid                           ),
    ("lazer_harp_lazer_mount.stl",              , lazerMount                    ),
    ("lazer_harp_lazer_mounts_positioned.stl"   , lazerMountsPositioned         )
  ]
main = do
    putStrLn "starting"
    void $ forM paths $ (\(path, part) -> do
        T.writeFile path $ Csg.STL.toSTL part
        putStrLn $ "written " <> path
      )

